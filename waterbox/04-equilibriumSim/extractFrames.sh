#!/bin/bash
source /usr/local/gromacs/GMXRC2022
source /usr/local/cuda-11.1/CUDA111RC

shopt -s -o nounset

gmx trjconv -f sim_100.xtc -o frames/sim_100.pdb -sep -s ./sim_100.tpr
